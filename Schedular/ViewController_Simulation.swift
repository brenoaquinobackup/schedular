//
//  ViewController_Simulation.swift
//  Schedular
//
//  Created by Breno Aquino on 10/04/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit

class SimuCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var begin: UILabel!
    @IBOutlet weak var end: UILabel!
    
}

class ViewController_Simulation: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var output_tableView: UITableView!
    var details: [(begin: Int, end: Int, id: Int, name: String)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        details = Schedular_System.instance.run(timeSlice: 4)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table View Data Source
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = output_tableView.dequeueReusableCell(withIdentifier: "SimuCell", for: indexPath) as! SimuCell
        cell.name.text = details[indexPath.row].name
        cell.begin.text = String(details[indexPath.row].begin)
        cell.end.text = String(details[indexPath.row].end)
        return cell
    }

}
