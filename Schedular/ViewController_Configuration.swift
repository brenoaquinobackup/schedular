//
//  ViewController_Configuration.swift
//  Schedular
//
//  Created by Breno Aquino on 09/04/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit

class AddCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var priority: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var begin: UILabel!
    
}

class ViewController_Configuration: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var input_name: UITextField!
    @IBOutlet weak var input_duration: UITextField!
    @IBOutlet weak var input_begin: UITextField!
    @IBOutlet weak var input_priority: UITextField!
    @IBOutlet weak var output_tableView: UITableView!
    
    var algorithm: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        algorithm = ["First-In First Out (FIFO)", "Shortest Job First (SJF)", "Scheduler by Priority No Preemptive", "Shortest Remaining Time Next (SRTN)", "Scheduler by Priority Preemptive", "Round-Robin", "Multilevel"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func add_process() {
        if input_name.text == "" || input_duration.text == "" || input_begin.text == "" || input_priority.text == "" {
            return
        }
        
        Schedular_System.instance.add_process(name: input_name.text!, duration: Int(input_duration.text!)!, begin: Int(input_begin.text!)!, priority: Int(input_priority.text!)!)
        
        output_tableView.reloadData()
        
        input_name.text = ""
        input_duration.text = ""
        input_begin.text = ""
        input_priority.text = ""
        
        input_name.resignFirstResponder()
        input_duration.resignFirstResponder()
        input_begin.resignFirstResponder()
        input_priority.resignFirstResponder()
    }
    
    /* Clear Simulation */
    @IBAction func clear_simulation() {
        let alert = UIAlertController(title: "Limpar Simulação", message: "Tem certeza que deseja apagar todos os processos?", preferredStyle: UIAlertControllerStyle.alert)
        
        let no_action = UIAlertAction(title: "Não", style: UIAlertActionStyle.default)
        let yes_action = UIAlertAction(title: "Sim", style: UIAlertActionStyle.destructive) { (ACTION) in
            Schedular_System.instance.clear()
            self.output_tableView.reloadData()
        }
        
        alert.addAction(no_action)
        alert.addAction(yes_action)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /* UITableViewDataSource */
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Schedular_System.instance.array_process.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = output_tableView.dequeueReusableCell(withIdentifier: "AddCell", for: indexPath) as! AddCell
        cell.name.text = Schedular_System.instance.array_process[indexPath.row].name
        cell.priority.text = String(Schedular_System.instance.array_process[indexPath.row].priority)
        cell.duration.text = String(Schedular_System.instance.array_process[indexPath.row].duration)
        cell.begin.text = String(Schedular_System.instance.array_process[indexPath.row].begin)
        return cell
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            Schedular_System.instance.array_process.remove(at: indexPath.row)
            output_tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    /* UIPickerViewDataSource */
    public func numberOfComponents(in pickerView: UIPickerView) -> Int { /* Numero de Colunas */
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return algorithm.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return algorithm[row]
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        Schedular_System.instance.schedularType = row
    }

}
