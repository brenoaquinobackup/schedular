//
//  Schedular_System.swift
//  Schedular
//
//  Created by Breno Aquino on 09/04/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import Foundation

class Schedular_System {
    
    static let instance = Schedular_System()
    var array_process: [Process]
    var CPU: Process
    var time: Int
    var schedularType: Int
    
    let First_In_First_Out_FIFO = 0
    let Shortest_Job_First_SJF = 1
    let Scheduler_by_Priority_No_Preemptive = 2
    let Shortest_Remaining_Time_Next_SRTN = 3
    let Scheduler_by_Priority_Preemptive = 4
    let Round_Robin = 5
    let Multilevel = 6
    
    init() {
        array_process = [Process]()
        CPU = Process()
        schedularType = 0
        time = 0
        
        /* Testes Rapidos */
        let Pages = Process(name: "A", id: 0, duration: 12, begin: 0, priority: 1)
        let Keynote = Process(name: "B", id: 1, duration: 3, begin: 5, priority: 3)
        let Numbers = Process(name: "C", id: 2, duration: 8, begin: 5, priority: 4)
        let Word = Process(name: "D", id: 3, duration: 4, begin: 10, priority: 3)
        let Excel = Process(name: "E", id: 4, duration: 7, begin: 15, priority: 2)
        array_process.append(Pages)
        array_process.append(Keynote)
        array_process.append(Numbers)
        array_process.append(Word)
        array_process.append(Excel)
        /* -------------- */
    }
    
    /* Configurações de Processo */
    func add_process(name _name_: String, duration _duration_: Int, begin _begin_: Int, priority _priority_: Int) {
        let toAdd = Process(name: _name_, id: array_process.count, duration: _duration_, begin: _begin_, priority: _priority_)
        array_process.append(toAdd)
    }
    
    func clear() {
        array_process.removeAll()
    }
    /* --- */
    
    /* Run */
    func run(timeSlice _timeSlice_: Int) -> [(Int, Int, Int, String)] {
        
        var details = [(begin: Int, end: Int, id: Int, name: String)]()
        
        switch schedularType {
        case First_In_First_Out_FIFO:
            details = first_in_first_out_fifo()
            
        case Shortest_Job_First_SJF:
            details = shortest_job_first_sjf()
        
        case Scheduler_by_Priority_No_Preemptive:
            details = scheduler_by_priority_no_preemptive()
            
        case Shortest_Remaining_Time_Next_SRTN:
            details = shortest_remaining_time_next_srtn()
            
        case Scheduler_by_Priority_Preemptive:
            details = scheduler_by_priority_preemptive()
            
        case Round_Robin:
            details = round_robin(timeSlice: _timeSlice_)
            
        case Multilevel:
            details = multilevel(timeSlice: _timeSlice_)
            
        default:
            break
        }
        
        return details
    }
    /* --- */
    
    /* Algoritmos de Escalonamento */
    func first_in_first_out_fifo() -> [(Int, Int, Int, String)] {
        
        time = 0
        var details = [(begin: Int, end: Int, id: Int, name: String)]()
        var process_run: [Process] = []
        var process_to_start: [Process] = []
        var process_end: [Process] = []
        var last_process = Process(name: "", id: -1, duration: 0, begin: 0, priority: 0)
        
        for proc in array_process {
            proc.duration_simulation = proc.duration
            
            if proc.begin == 0 {
                process_run.append(proc)
            }
            else {
                process_to_start.append(proc)
            }
        }
        
        var toAdd: (begin: Int, end: Int, id: Int, name: String) = (0, 0, 0, "")
        while process_end.count < array_process.count {
            if process_run.count > 0 {
                if time >= process_run[0].begin {
                    CPU = process_run[0]

                    toAdd.begin = time
                    time += CPU.duration_simulation
                    CPU.duration_simulation = 0
                    toAdd.end = time
                    
                    process_run[0] = CPU
                    
                    toAdd.id = CPU.id
                    toAdd.name = CPU.name
                    details.append(toAdd)
                    
                    process_end.append(process_run[0])
                    process_run.remove(at: 0)
                    
                    if last_process.id != CPU.id {
                        time += 2
                    }
                    
                    last_process = CPU
                }
            }
            else {
                time += 1
            }
            
            // Verifica se chegou no tempo de algum processo entrar
            if process_to_start.count > 0 {
                var ended = false
                while !ended {
                    for i in 0...(process_to_start.count-1) {
                        if time >= process_to_start[i].begin {
                            process_run.append(process_to_start[i])
                            process_to_start.remove(at: i)
                            
                            if process_to_start.count == 0 {
                                ended = true
                            }
                            
                            break;
                        }
                        
                        if i == (process_to_start.count - 1) {
                            ended = true
                        }
                    }
                    
                }
            }
            // ---
        }
        
        return details
        
    } /* Feito - Refatorado - Testado */
    
    func shortest_job_first_sjf() -> [(Int, Int, Int, String)] {

        time = 0
        var details = [(begin: Int, end: Int, id: Int, name: String)]()
        var process_run: [Process] = []
        var process_to_start: [Process] = []
        var process_end: [Process] = []
        var last_process = Process(name: "", id: -1, duration: 0, begin: 0, priority: 0)
        
        for proc in array_process {
            proc.duration_simulation = proc.duration
            
            if proc.begin == 0 {
                process_run.append(proc)
            }
            else {
                process_to_start.append(proc)
            }
        }
        
        var toAdd: (begin: Int, end: Int, id: Int, name: String) = (0, 0, 0, "")
        while process_end.count < array_process.count {
            
            if process_run.count > 0 {
                // Para descobrir o processo com o menor tempo
                var shorter_process: Int = 0            /* Guarda o indice do processo com menor tempo */
                var shorter_duration: Int = 999
                
                for j in 0...(process_run.count-1) {
                    if process_run[j].duration_simulation < shorter_duration {
                        shorter_duration = process_run[j].duration_simulation
                        shorter_process = j
                    }
                }
                // ---
                
                CPU = process_run[shorter_process]
                
                toAdd.begin = time
                time += CPU.duration_simulation
                CPU.duration_simulation = 0
                toAdd.end = time
                
                process_run[shorter_process] = CPU
                
                toAdd.id = CPU.id
                toAdd.name = CPU.name
                details.append(toAdd)
                
                // Retira o processo da fila de run e coloca em end
                process_end.append(process_run[shorter_process])
                process_run.remove(at: shorter_process)
                
                if last_process.id != CPU.id {
                    time += 2
                }
                
                last_process = CPU
            }
            else {
                time += 1
            }
            
            // Verifica se chegou no tempo de algum processo entrar
            if process_to_start.count > 0 {
                var ended = false
                while !ended {
                    for i in 0...(process_to_start.count-1) {
                        if time >= process_to_start[i].begin {
                            process_run.append(process_to_start[i])
                            process_to_start.remove(at: i)
                            
                            if process_to_start.count == 0 {
                                ended = true
                            }
                            
                            break;
                        }
                        
                        if i == (process_to_start.count - 1) {
                            ended = true
                        }
                    }
                    
                }
            }
            // ---
        }
        
        return details
        
    } /* Feito - Refatorado - "Testado" */
    
    func scheduler_by_priority_no_preemptive() -> [(Int, Int, Int, String)] {
        
        time = 0
        var details = [(begin: Int, end: Int, id: Int, name: String)]()
        var process_run: [Process] = []
        var process_to_start: [Process] = []
        var process_end: [Process] = []
        var last_process = Process(name: "", id: -1, duration: 0, begin: 0, priority: 0)
        
        for proc in array_process {
            proc.duration_simulation = proc.duration
            
            if proc.begin == 0 {
                process_run.append(proc)
            }
            else {
                process_to_start.append(proc)
            }
        }
        
        var toAdd: (begin: Int, end: Int, id: Int, name: String) = (0, 0, 0, "")
        while process_end.count < array_process.count {
            
            if process_run.count > 0 {
                // Para descobrir se ha algum processo na fila com a prioridade maior
                var higher_priority_process: Int = 0            /* Guarda o indice do processo com maior prioridade */
                var higher_priority: Int = 0
                
                for j in 0...(process_run.count-1) {
                    if process_run[j].priority > higher_priority {
                        higher_priority = process_run[j].priority
                        higher_priority_process = j
                    }
                }
                // ---
                    
                CPU = process_run[higher_priority_process]
                
                toAdd.begin = time
                time += CPU.duration_simulation
                CPU.duration_simulation = 0
                toAdd.end = time
                
                process_run[higher_priority_process] = CPU
                
                toAdd.id = CPU.id
                toAdd.name = CPU.name
                details.append(toAdd)
                
                // Retira o processo da fila de run e coloca em end
                process_end.append(process_run[higher_priority_process])
                process_run.remove(at: higher_priority_process)

                if last_process.id != CPU.id {
                    time += 2
                }
                
                last_process = CPU
            }
            else {
                time += 1
            }
            
            // Verifica se chegou no tempo de algum processo entrar
            if process_to_start.count > 0 {
                var ended = false
                while !ended {
                    for i in 0...(process_to_start.count-1) {
                        if time >= process_to_start[i].begin {
                            process_run.append(process_to_start[i])
                            process_to_start.remove(at: i)
                            
                            if process_to_start.count == 0 {
                                ended = true
                            }
                            
                            break;
                        }
                        
                        if i == (process_to_start.count - 1) {
                            ended = true
                        }
                    }
                    
                }
            }
            // ---
        }
        
        return details
        
    } /* Feito - Refatorado - "Testado" */
    
    func shortest_remaining_time_next_srtn() -> [(Int, Int, Int, String)] {
        time = 0
        var details = [(begin: Int, end: Int, id: Int, name: String)]()
        var process_run: [Process] = []
        var process_to_start: [Process] = []
        var process_end: [Process] = []
        var last_process: Process = Process()
        var isFirst: Bool = true
        
        for proc in array_process {
            proc.duration_simulation = proc.duration
            
            if proc.begin == 0 {
                process_run.append(proc)
            }
            else {
                process_to_start.append(proc)
            }
        }
        
        var toAdd: (begin: Int, end: Int, id: Int, name: String) = (0, 0, 0, "")
        while process_end.count < array_process.count {
            
            if process_run.count > 0 {
                // Para descobrir o processo com o menor tempo
                var shorter_process: Int = 0            /* Guarda o indice do processo com menor tempo */
                var shorter_duration: Int = 999
                
                for j in 0...(process_run.count-1) {
                    if process_run[j].duration_simulation < shorter_duration {
                        shorter_duration = process_run[j].duration_simulation
                        shorter_process = j
                    }
                }
                
                CPU = process_run[shorter_process]
                
                if isFirst {
                    toAdd.id = CPU.id
                    toAdd.name = CPU.name
                    toAdd.begin = time
                    isFirst = false
                }
                else if CPU.id != last_process.id {
                    toAdd.end = time
                    details.append(toAdd)
                    
                    /**/
                    time += 2
                    
                    toAdd.begin = time
                    toAdd.id = CPU.id
                    toAdd.name = CPU.name
                    
                }
                
                last_process = CPU

                
                // Tem que subir de 1 em 1 pra verificar se algum processo chegou
                time += 1
                CPU.duration_simulation! -= 1
                // ---
                
                // Verifica se o processo que estava na CPU foi concluido
                if CPU.duration_simulation == 0 {
                    process_end.append(process_run[shorter_process])
                    process_run.remove(at: shorter_process)
                    
                    // Se ele foi concluido e ele é o ultimo processo que falta
                    // ele entra nesse if (é necessario fazer isso pois como nao
                    // havera mais processo, nao voltara pro comeco desse while,
                    // ou seja, nao setaria o end e nao adicionaria (if anterior))
                    if process_run.count == 0 {
                        toAdd.end = time
                        details.append(toAdd)
                    }
                }
                // ---
            }
            else {
                time += 1
                isFirst = true
            }
            
            // Verifica se chegou no tempo de algum processo entrar
            if process_to_start.count > 0 {
                var ended = false
                while !ended {
                    for i in 0...(process_to_start.count-1) {
                        if time >= process_to_start[i].begin {
                            process_run.append(process_to_start[i])
                            process_to_start.remove(at: i)
                            
                            if process_to_start.count == 0 {
                                ended = true
                            }
                            
                            break;
                        }
                        
                        if i == (process_to_start.count - 1) {
                            ended = true
                        }
                    }
                    
                }
            }
            // ---
        }

        return details
    } /* Feito - Refatorado - "Testado" */
    
    func scheduler_by_priority_preemptive() -> [(Int, Int, Int, String)] {
        time = 0
        var details = [(begin: Int, end: Int, id: Int, name: String)]()
        var process_run: [Process] = []
        var process_to_start: [Process] = []
        var process_end: [Process] = []
        var last_process: Process = Process()
        var isFirst: Bool = true
        
        for proc in array_process {
            proc.duration_simulation = proc.duration
            
            if proc.begin == 0 {
                process_run.append(proc)
            }
            else {
                process_to_start.append(proc)
            }
        }
        
        var toAdd: (begin: Int, end: Int, id: Int, name: String) = (0, 0, 0, "")
        while process_end.count < array_process.count {
            if process_run.count > 0 {
                // Para descobrir se ha algum processo na fila com a prioridade maior
                var higher_priority_process: Int = 0            /* Guarda o indice do processo com maior prioridade */
                var higher_priority: Int = 0
                
                for j in 0...(process_run.count-1) {
                    if process_run[j].priority > higher_priority {
                        higher_priority = process_run[j].priority
                        higher_priority_process = j
                    }
                }
                // ---
                    
                CPU = process_run[higher_priority_process]
                
                if isFirst {
                    toAdd.id = CPU.id
                    toAdd.name = CPU.name
                    toAdd.begin = time
                    isFirst = false
                }
                else if CPU.id != last_process.id {
                    toAdd.end = time
                    details.append(toAdd)
                    
                    /**/
                    time += 2
                    
                    toAdd.begin = time
                    toAdd.id = CPU.id
                    toAdd.name = CPU.name
                    
                }
                
                last_process = CPU
                
                // Tem que subir de 1 em 1 pra verificar se algum processo chegou
                time += 1
                CPU.duration_simulation! -= 1
                // ---
                
                // Verifica se o processo que estava na CPU foi concluido
                if CPU.duration_simulation == 0 {
                    process_end.append(process_run[higher_priority_process])
                    process_run.remove(at: higher_priority_process)
                    
                    // Se ele foi concluido e ele é o ultimo processo que falta
                    // ele entra nesse if (é necessario fazer isso pois como nao
                    // havera mais processo, nao voltara pro comeco desse while,
                    // ou seja, nao setaria o end e nao adicionaria (if anterior))
                    if process_run.count == 0 {
                        toAdd.end = time
                        details.append(toAdd)
                    }
                }
                // ---
            }
            else {
                time += 1
                isFirst = true
            }
            
            // Verifica se chegou no tempo de algum processo entrar
            if process_to_start.count > 0 {
                var ended = false
                while !ended {
                    for i in 0...(process_to_start.count-1) {
                        if time >= process_to_start[i].begin {
                            process_run.append(process_to_start[i])
                            process_to_start.remove(at: i)
                            
                            if process_to_start.count == 0 {
                                ended = true
                            }
                            
                            break;
                        }
                        
                        if i == (process_to_start.count - 1) {
                            ended = true
                        }
                    }
                    
                }
            }
            // ---
        }
        
        return details
    } /* Feito - Refatorado - "Testado" */
    
    func round_robin(timeSlice _timeSlice_: Int) -> [(Int, Int, Int, String)] {
        time = 0
        var details = [(begin: Int, end: Int, id: Int, name: String)]()
        var process_run: [Process] = []
        var process_to_start: [Process] = []
        var process_end: [Process] = []
        var i: Int = 0
        var last_process = Process(name: "", id: -1, duration: 0, begin: 0, priority: 0)

        for proc in array_process {
            proc.duration_simulation = proc.duration
            
            if proc.begin == 0 {
                process_run.append(proc)
            }
            else {
                process_to_start.append(proc)
            }
        }
        
        last_process = process_run[0]
        
        var toAdd: (begin: Int, end: Int, id: Int, name: String) = (0, 0, 0, "")
        while process_end.count < array_process.count {
            if process_run.count > 0 {
                CPU = process_run[i]
                
                toAdd.begin = time
                if CPU.duration_simulation > _timeSlice_ {
                    time += _timeSlice_
                    CPU.duration_simulation! -= _timeSlice_
                }
                else {
                    time += CPU.duration_simulation
                    CPU.duration_simulation = 0
                    
                    process_end.append(process_run[i])
                    process_run.remove(at: i)
                }
                toAdd.end = time
                
                toAdd.id = CPU.id
                toAdd.name = CPU.name
                details.append(toAdd)
                
                i += 1
                if i >= process_run.count {
                    i = 0
                }
                
                if last_process.id != CPU.id {
                    time += 2
                }
                
                last_process = CPU;
            }
            else {
                time += 1
            }
            
            // Verifica se chegou no tempo de algum processo entrar
            if process_to_start.count > 0 {
                var ended = false
                while !ended {
                    for i in 0...(process_to_start.count-1) {
                        if time >= process_to_start[i].begin {
                            process_run.append(process_to_start[i])
                            process_to_start.remove(at: i)
                            
                            if process_to_start.count == 0 {
                                ended = true
                            }
                            
                            break;
                        }
                        
                        if i == (process_to_start.count - 1) {
                            ended = true
                        }
                    }
                    
                }
            }
            // ---
        }

        return details
    } /* Feito - Refatorado - "Testado" */
    
    func multilevel(timeSlice _timeSlice_: Int) -> [(Int, Int, Int, String)] {

        var details = [(begin: Int, end: Int, id: Int, name: String)]()
        return details
        
    } /* Para Fazer */
    /* --- */
}
