//
//  Process.swift
//  Schedular
//
//  Created by Breno Aquino on 09/04/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import Foundation

class Process {
    var name: String!
    var id: Int!
    var duration: Int!
    var begin: Int!
    var priority: Int!
    var duration_simulation: Int!
    
    init() {
        name = ""
        id = 0
        duration = 0
        duration_simulation = 0
        begin = 0
        priority = 0
    }
    
    init(name _name_: String, id _id_: Int, duration _duration_: Int, begin _begin_: Int, priority _priority_: Int) {
        self.name = _name_
        self.id = _id_;
        self.duration = _duration_
        self.duration_simulation = _duration_
        self.begin = _begin_
        self.priority = _priority_
    }
}
